

all: compile


compile: ffibers.fnl sieve.fnl
	./fennel --compile ffibers.fnl > ffibers.lua
	./fennel --compile sieve.fnl > sieve.lua


test: ffibers.fnl sieve.fnl
	./fennel sieve.fnl
