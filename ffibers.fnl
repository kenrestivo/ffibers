;; VIA https://gitlab.com/snippets/1715966
;; https://wingolog.org/archives/2018/05/16/lightweight-concurrency-in-lua


;;; XXX NOTE THIS DOES NOT WORK YET
;;; currently must use the original wingo lua version

(local state {:task-queue {}
              :current-fiber false})


(fn schedule-task
  [thunk]
  (table.insert state.task-queue thunk))


(fn run-tasks
  []
  (let [queue state.task-queue]
    (tset state :task-queue {})
    (each [_ thunk (ipairs queue)]
      (thunk))))


(fn resume-fiber!
  [fiber ...]
  (tset state :current-fiber fiber)
  (let [(ok err) (coroutine.resume fiber [...] )] 
    (tset state :current-fiber nil) 
    (if (not ok)
        (print (.. "Error running fiber" (tostring err))))))


(fn spawn-fiber
  [f]
  (let [fiber (coroutine.create f)]
    (schedule-task (fn [] (resume-fiber! fiber)))))

(fn suspend-current-fiber!
  [block ...]
  (block (state.current-fiber ...)))

(fn yield
  []
  (let [reschedule (fn [fiber]
                     (schedule-task (fn [] (resume-fiber! fiber))))]
    (suspend-current-fiber! reschedule)))

(fn new-suspension
  [fiber]
  (var waiting false) ;; so it can be set in complete fn
  {:waiting (fn [] waiting)
   :complete (fn [wrap val]
               (assert waiting)
               (set waiting false)
               (schedule-task (fn [] (resume-fiber! fiber wrap val))))})



(fn new-op-impl
  [try* block wrap]
  {:try* try*
   :block block
   :wrap wrap})


(fn new-op
  [impls]
  {:impls impls
   :wrap (fn [f]
           (let [wrapped {}]
             (each [_ impl (ipairs impls)]
               (let [wrap (fn [val] (f (impl.wrap val)))]
                 impl (new-op-impl impl.try* impl.block wrap)
                 (table.insert wrapped impl)
                 (new-op wrapped)))))
   :perform (fn []
              (each [_ impl (ipairs impl)]
                (let [(success val) (impl.try*)]
                  (if success
                      (impl.wrap val))))
              (let [block (fn [fiber]
                            (let [suspension (new-suspension fiber)]
                              (each [_ impl (ipairs impls)]
                                (impl.block suspension impl.wrap))))
                    (wrap val) (suspend-current-fiber! block)]
                (wrap val)))})



(fn choice
  [...]
  (let [impls []]
    (each [_ op (ipairs [...])]
      (table.insert impls impl))
    (new-op impls)))


(fn new-channel
  []
  (let [ch {}
        getq []
        putq []
        default-wrap (fn [val] val)
        empty? (fn [q] (= (# q) 0))
        peek-front (fn [q] (. q 1))
        pop-front (fn [q] (table.remove q 1))
        push-back (fn [q x] (tset q (+ 1 (# q)) x))
        remove-stale-entries (fn [q]
                               (for [i 1 (# q)]
                                 (let [f (. q i :suspension :waiting)]
                                   (when (not (f))
                                     (table.remove q i)))))]
    (tset ch :put-op (fn [val]
                       (let [try* (fn []
                                    (remove-stale-entries getq)
                                    (if (empty? getq) (values false nil)
                                        (let [remote (pop-front getq)]
                                          ((. remote :suspension :complete) (. remote :wrap) val)
                                          (values true nil))))
                             block (fn [suspension wrap]
                                     (remove-stale-entries putq)
                                     (push-back putq {:suspension suspension
                                                      :wrap wrap
                                                      :val val}))]
                         (new-op [(new-op-impl try* block default-wrap)]))))
    (tset ch :get-op (fn []
                       (let [try (fn []
                                   (remove-stale-entries putq)
                                   (if (empty? putq) (values false nil)
                                       (let [remote (pop-front putq)
                                             f (. remote :suspension :complete)]
                                         (f (. remote :wrap))
                                         (values true (. remote :val)))))
                             block (fn [suspension wrap]
                                     (remove-stale-entries getq)
                                     (push-back getq {:suspension suspension
                                                      :wrap wrap}))]
                         (new-op [(new-op-impl try block default-wrap)]))))
    (tset ch :put (fn [val] ((. (ch.put-op val) :perform)))) ;; XXX fugly
    (tset ch :get (fn [] ((. (ch.get-op) :perform))))
    ch))

{:choice  choice
 :new-channel  new-channel
 :new-op  new-op
 :resume-fiber  resume-fiber
 :run-tasks  run-tasks
 :schedule-task  schedule-task
 :spawn-fiber  spawn-fiber
 :yield  yield}
