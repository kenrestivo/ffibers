;;  VIA https://gitlab.com/snippets/1715966
;; https://wingolog.org/archives/2018/05/16/lightweight-concurrency-in-lua

;; (local ff (require "ffibers"))
(local ffibers (require "wingo-fibers"))
(local ff {:new-channel ffibers.new_channel
           :spawn-fiber ffibers.spawn_fiber
           :run-tasks ffibers.run_tasks})


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Application functions

(fn put-integers
  ;; Takes a channel tx and a number to start with.
  ;; Puts integers incrementally into channel, endlessly
  ;; This is basically clojure's (range) lazy seq into a channel
  [tx n]
  (tx.put n)
  (put-integers tx (+ n 1)))


(fn integers-fiber
  ;; Takes a number. Starts a fiber.
  ;; Returns a channel into which incremental integers will be put forever
  [n]
  (let [tx (ff.new-channel)]
    (ff.spawn-fiber #(put-integers tx n))
    tx))



(fn filter-sieve
  ;; Takes a tx channel and an rx channel, and a prime p.
  ;; Pulls integers out of rx endlessly, and puts them into tx only if
  ;; they are not evenly divisible by p
  [tx rx p]
  (let [n (rx.get)]
    (when (not (= 0 (% n p)))
      (tx.put n)))
  (filter-sieve tx rx p))



(fn sieve-fiber
  ;; Takes in integer i and an rx channel.
  ;; Starts a fiber that endlessly takes numbers out of rx channel,
  ;; and puts them into a tx channel if they are not evenly divisible by i.
  ;; Returns the tx channel.
  [i rx]
  (let [tx (ff.new-channel)]
    (ff.spawn-fiber #(filter-sieve tx rx i))
    tx))

;; XXX this docstring sucks
(fn put-primes
  [tx rx]
  ;; Takes a tx channel and an rx channel.
  ;; Endlessly pulls primes out of rx, puts them into tx,
  ;; and then pull numbers out of a sieve channel
  ;; starting with that prime, and separate fiber,
  ;; putting those into tx channel, recursively.
  (let [p (rx.get)]
    (tx.put p) 
    (put-primes tx (sieve-fiber p rx))))



(fn primes-fiber
  ;; Creates a fiber for returning primes,
  ;; which in turn creates a fiber for returning integers starting with 2.
  ;; Returns a channel that endlessly returns primes.
  []
  (let [tx (ff.new-channel)]
    (ff.spawn-fiber #(put-primes tx (integers-fiber 2)))
    tx))


(fn print-results!
  ;; Given a state table and a count at which to stop,
  ;; creates a primes fiber and sieve,
  ;; and pulls primes from an rx channel until count is reached.
  [state count]
  (let [rx (primes-fiber)]
    (for [i 1 count] 
      (print (rx.get)))
    (tset state :done true)))


(fn print-results-fiber
  ;; Given a state table and count to stop at,
  ;; creates a print results fiber, which prints the results
  ;; of the sieve until count is reached.
  [state count]
  (ff.spawn-fiber #(print-results! state count)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main

(fn prime-sieve
  ;; Main function. 1
  ;; Takes a count to stop at,
  ;; prints results of prime sieve up until count.
  [count]
  (let [state {:done false}]
    (print-results-fiber state count)
    (while (not state.done)
      (ff.run-tasks))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; side effects

(prime-sieve 100)


