-- VIA https://gitlab.com/snippets/1715966
-- https://wingolog.org/archives/2018/05/16/lightweight-concurrency-in-lua


function prime_sieve(count)
   
   local function sieve(p, rx)
      local tx = new_channel()
      spawn_fiber(function ()
	    while true do
	       local n = rx.get()
	       if n % p ~= 0 then tx.put(n) end
	    end
      end)
      return tx
   end

   local function integers_from(n)
      local tx = new_channel()
      spawn_fiber(function ()
	    while true do
	       tx.put(n)
	       n = n + 1
	    end
      end)
      return tx
   end

   local function primes()
      local tx = new_channel()
      spawn_fiber(function ()
	    local rx = integers_from(2)
	    while true do
	       local p = rx.get()
	       tx.put(p)
	       rx = sieve(p, rx)
	    end
      end)
      return tx
   end

   local done = false
   spawn_fiber(function()
	 local rx = primes()
	 for i=1,count do print(rx.get()) end
	 done = true
   end)

   while not done do run_tasks() end
end

prime_sieve(100)
